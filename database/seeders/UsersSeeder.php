<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
//            'first_name' => 'Super',
//            'last_name' => 'Admin',
            'email' => 'superadmin@admin.com',
            'email_verified_at' => now(),
            'password' => bcrypt('password'),
            'user_type' => User::USER_TYPE_ADMIN,
        ]);
    }
}
