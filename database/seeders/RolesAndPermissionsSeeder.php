<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'create admin']);
        Permission::create(['name' => 'delete admin']);

        Permission::create(['name' => 'create company']);
        Permission::create(['name' => 'view company']);
        Permission::create(['name' => 'view all companies']);

        Permission::create(['name' => 'view all employee']);
        Permission::create(['name' => 'create employee']);
        Permission::create(['name' => 'view all company employee']);


        // create roles and assign created permissions

        // this can be done as separate statements
        $superAdminRole = Role::create(['name' => 'super admin']);
        $adminRole = Role::create(['name' => 'admin']);
        $companyRole = Role::create(['name' => 'company']);
        $employeeRole = Role::create(['name' => 'employee']);

        $superAdminRole->givePermissionTo(['create user', 'create admin', 'delete admin', 'delete user', 'view user', 'view all companies', 'view all company employee']);
        $adminRole->givePermissionTo(['create user', 'view all companies', 'view all company employee', 'view user']);
        $companyRole->givePermissionTo(['create employee', 'view all employee']);

        User::query()->first()->assignRole("super admin");



//        $role = Role::create(['name' => 'super-admin']);
//        $role->givePermissionTo(Permission::all());
        $this->command->info("Roles and Permission seeded successfully");
    }
}
