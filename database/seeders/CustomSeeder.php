<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CustomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $companyRole = Role::whereName('company')
//            ->first()
////            ->givePermissionTo(['create employee', 'view all employee'])
//            ->revokePermissionTo('create user')
//        ;

        $superAdminRole = Role::whereName('super admin')
            ->first()
            ->givePermissionTo(['view user'])
//            ->revokePermissionTo('create user')
        ;


    }
}
