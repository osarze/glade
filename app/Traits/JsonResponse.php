<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use \Illuminate\Http\JsonResponse as HttpJsonResponse;

trait JsonResponse
{
    /**
     * @param string $message
     * @param array $data
     * @param int $successCode
     *
     * @return HttpJsonResponse
     */
    public function successResponse(
        ?string $message = "Successful",
        array | Model | Collection $data = [],
        int $successCode = Response::HTTP_OK
    ): HttpJsonResponse
    {
        return response()->json([
            "success" => true,
            "message"=> $message,
            "data" => $data,
        ], $successCode);
    }

    public function unAuthorizeResponse($message = "Unauthorized"): HttpJsonResponse
    {
        return response()->json([
            "success" => false,
            "message"=> $message,
        ], Response::HTTP_UNAUTHORIZED);
    }

    public function resourceSuccess(string $message, JsonResource $resourceData, $successCode = 200)
    {
        $resourceData = $resourceData->response()->getData(true);

        $responseData = [
            'status' => 'success',
            'statusCode' => config('errors.codes.ok'),
            'message' => $message,
            'data' => $resourceData['data'],
        ];
        if (Arr::has($resourceData, 'meta')){
            $responseData['meta'] = $resourceData['meta'];
        }

        if (Arr::has($resourceData, 'links')){
            $responseData['links'] = $resourceData['links'];
        }

        return response()->json($responseData, $successCode);

//        return \Illuminate\Support\Facades\Response::json($responseData, config('errors.codes.ok'), $headers);
    }

    public function errorResponse(
        ?string $message = "An error occur",
        array $errorData = [],
        int $errorCode = Response::HTTP_BAD_REQUEST
    ): HttpJsonResponse
    {
        return response()->json([
            "success" => false,
            "message"=> $message,
            "error" => $errorData,
        ], $errorCode);
    }

//    public function paginationResponse(LengthAwarePaginator $paginatedData, ?string $message, $successCode=200)
//    {
//        return response()->json([
//            "success" => true,
//            "message"=> $message,
//            "data" => $paginatedData->items(),
//            'links' => $paginatedData->linkCollection()
//        ], $successCode);
//    }
}
