<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\Employee\EmployeeCollection;
use App\Http\Resources\Employee\EmployeeResource;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->user_type == User::USER_TYPE_COMPANY) {
            $employees = Employee::where('company_id', auth()->user()->employee->company_id);
        }else {
            $employees = Employee::query();
        }
        $employees = $employees->paginate(10);


        return $this->resourceSuccess("Employee list fetched successfully", new EmployeeCollection($employees));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        try {
            DB::beginTransaction();
            $userId = null;
            $user = new User();
//            $user->first_name = $request->first_name;
//            $user->last_name = $request->last_name;

            if ($request->has("email")){
                $user->email = $request->email;
                $user->password = bcrypt("password");
                $user->user_type = User::USER_TYPE_EMPLOYEE;
                $user->save();

                $userId = $user->id;
            }

            $employee = new Employee();
            $employee->user_id = $userId;
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->email = $request->email;
            $employee->phone = $request->phone_no;
            $employee->company_id = (auth()->user()->user_type == User::USER_TYPE_COMPANY) ? auth()->user()->employee->company_id : $request->company_id;
            $employee->save();

            DB::commit();
//            dd($employee);

            return $this->resourceSuccess("Employee created successfully", new EmployeeResource($employee));
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            Log::error($e);
        }

        return $this->errorResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return $this->resourceSuccess("Employee details fetched successfully", new EmployeeResource($employee));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
//        $employee->company_id = $request->company_id;

        $employee->save();

        return $this->successResponse("Employee updated successfully", $employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return $this->successResponse("Employee deleted successfully");
    }
}
