<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Http\Resources\Company\CompanyCollection;
use App\Http\Resources\Company\CompanyResource;
use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return $this->resourceSuccess("List of Companies fetched successfully", new CompanyCollection($companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyRequest $request
     * @return JsonResponse
     */
    public function store(CompanyRequest $request)
    {
        $company = new Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->created_by = auth()->user()->id;
        $company->save();

        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            $path = $request->logo->store('logo', 'public');
            $company->logo = $path;
            $company->save();
        }

        return $this->resourceSuccess("Company created successfully", new CompanyResource($company));
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function show(Company $company)
    {
        return $this->resourceSuccess("", new CompanyResource($company));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompanyRequest $request
     * @param Company $company
     * @return JsonResponse
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->save();

        if (
            $request->hasFile('logo')
            && $request->file('logo')->isValid()
        ) {
            if ($company->logo && Storage::disk('public')->exists($company->logo)) {
                Storage::disk('public')->delete($company->logo);
            }

            $path = $request->logo->store('logo', 'public');
            $company->logo = $path;
            $company->save();
        }

        return $this->successResponse("Companies details updated successfully", $company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Company  $company
     * @return JsonResponse
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return $this->successResponse("Company Details deleted successfully");
    }

    public function myCompanyDetails()
    {
        $company = Company::find(!auth()->user()->employee ? null : auth()->user()->employee->company_id);
//
        return $this->resourceSuccess("", new CompanyResource($company));
    }
}
