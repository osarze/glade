<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['email','required'],
            'password' => ['required', 'min:8']
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            return $this->unAuthorizeResponse("Invalid Login Details");
        }

        $user = auth()->user();
        $tokenResult = $user->createToken('authToken')->plainTextToken;

        return $this->successResponse("Login Successful", [
            'access_token' => $tokenResult,
            'token_type' => 'Bearer',
            'user' => $user,
            'permissions' => $user->getAllPermissions()->pluck('name')
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return $this->success("Logout successfully");
    }
}
