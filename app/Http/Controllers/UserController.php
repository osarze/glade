<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\Employee\EmployeeCollection;
use App\Http\Resources\User\UserCollection;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $users = User::paginate(10);

        return $this->resourceSuccess("users listr fetched successfully", new UserCollection($users));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     * @return JsonResponse
     */
    public function store(UserRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = new User();
//            $user->first_name = $request->first_name;
//            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = bcrypt("password");
            $user->user_type = $request->user_type;
            $user->save();

            $employee = new Employee();
            $employee->user_id = $user->id;
            $employee->company_id = $request->company_id;
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->save();


            $user->assignRole(User::ROLES[$user->user_type]);

            DB::commit();

            return $this->successResponse("User Account created successfully",$user);
        } catch (\Exception $e) {
            dd($e);
            DB::rollBack();
            Log::error($e);
        }

        return $this->errorResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return JsonResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return $this->successResponse("Employee deleted successfully");
    }
}
