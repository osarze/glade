<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:190'],
            'last_name' => ['required', 'max:190'],
            'user_type' => ['required', 'in:'. User::USER_TYPE_COMPANY . ',' . User::USER_TYPE_ADMIN . ',' . User::USER_TYPE_EMPLOYEE],
            'email' => ['required', 'max:190', 'unique:users,email'],
            'company_id' => ['nullable', 'required_if:user_type,' . User::USER_TYPE_COMPANY . ',' . User::USER_TYPE_EMPLOYEE, 'exists:companies,id']
        ];
    }
}
