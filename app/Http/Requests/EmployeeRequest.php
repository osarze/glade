<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $employeeId = $this->route('employee') ? $this->route('employee')->id : null;

        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'email' => ['nullable', 'email', 'unique:employees,email,' . $employeeId ],
            'phone' => ['nullable', 'digits:11', 'unique:employees,email,' . $employeeId],
            'company_id' => ['nullable', Rule::requiredIf(auth()->user()->user_type != User::USER_TYPE_COMPANY), 'exists:companies,id'],
        ];
    }
}
